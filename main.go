package main

import (
	"context"
	"fmt"
	"math/rand"
	"time"

	"github.com/kfkgo/db"
	"github.com/segmentio/kafka-go"
)

type User struct {
	Name string `json:"name" xml:"name" form:"name"`
}

const (
	PRIME_TOPIC = "PRIME_TOPIC"
)

func newKafkaProducer(kafkaUrl string) *kafka.Writer {
	return &kafka.Writer{
		Addr:                   kafka.TCP(kafkaUrl),
		Balancer:               &kafka.LeastBytes{},
		Topic:                  PRIME_TOPIC,
		AllowAutoTopicCreation: true,
	}
}

func main() {

	producer := newKafkaProducer("localhost:29092")

	defer producer.Close()

	p := new(User)
	p.Name = "malta"
	go db.WriteFile("postgres.txt", 1, string(p.Name))

	r := rand.New(rand.NewSource(99))
	key := fmt.Sprintf("key-%d", r)
	time.Sleep(2 * time.Second)
	err := producer.WriteMessages(context.Background(),
		kafka.Message{
			Key:   []byte(key),
			Value: []byte(p.Name),
		},
	)

	if err != nil {
		fmt.Println(err)
	} else {
		fmt.Print("New message: ", key)
	}
}
