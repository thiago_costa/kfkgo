package main

import (
	"context"
	"fmt"
	"log"

	"github.com/kfkgo/db"
	"github.com/segmentio/kafka-go"
)

const (
	PRIME_TOPIC = "PRIME_TOPIC"
)

func newKafkaConsumer(kafkaUrl, groupId, topic string) *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers:  []string{kafkaUrl},
		GroupID:  groupId,
		Topic:    topic,
		MinBytes: 10e1, // 10kb
		MaxBytes: 10e6, // 10MB
	})
}

func consume() {
	consumer := newKafkaConsumer("localhost:29092", "go", PRIME_TOPIC)
	fmt.Println("consumer")
	defer consumer.Close()

	for {
		msg, err := consumer.ReadMessage(context.Background())
		go db.WriteFile("elasticsearch.json", 5, string(msg.Value))
		if err != nil {
			break
		}
		fmt.Println("new message received with key: ", string(msg.Key))
	}
	if err := consumer.Close(); err != nil {
		log.Fatal("failed to close reader: ", err)
	}
}

func main() {
	consume()
}
